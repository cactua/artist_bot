const fs = require('fs')
const TelegramBot = require('node-telegram-bot-api');
const token = '1929717260:AAGymckJWr9wmYFCQeH0bFXnMcU2l_hfVEc'; //telegram token
global.bot = new TelegramBot(token, { polling: true }); //bot instance
global.dbFile = process.cwd() + '/db/artists.db'
global.sqlite3 = require('sqlite3').verbose();
global.db = new sqlite3.Database(dbFile)
let fn = require('./src/functions/functions.js').default;
require('./src/functions/messages')


const dirs = ['./src/init', './src/commands'];
dirs.forEach(dir => {
    fs.readdir(dir, function (err, filenames) {
        filenames.forEach(function (filename) {
            let filePath = dir + "/" + filename
            require(filePath)
        });
    });
})

bot.on("polling_error", console.log);

// Listen for any kind of message. There are different kinds of
// messages.
bot.on('message', async (msg) => {
    if (msg.text.startsWith("/")) return
    let user = msg.from
    if (!fn) {
        fn = require('./src/functions/functions.js').default;
    }
    let check = await fn.haveState(user)
    if (check) {
        fn.getState(user).then(row => {
            let command = row.command
            if (command === "/setname") {
                fn.setName(user, msg.text)
            }
        })
    }
});