const sqlite3 = require('sqlite3').verbose();
const fs = require('fs')
const db = new sqlite3.Database(dbFile);
db.serialize(function () {
    db.run(`
        CREATE TABLE IF NOT EXISTS users
            (
                id BIGINT PRIMARY KEY,
                telegram_tag TEXT NOT NULL,
                artist_name,
                is_open BOOL default 0
            )`
    );

    db.run(`
        CREATE TABLE IF NOT EXISTS followers
            (
                user BIGINT NOT NULL,
                artist BIGINT NOT NULL,
                CONSTRAINT fk_followers 
                    FOREIGN KEY (user) REFERENCES users(id)
                ,
                CONSTRAINT fk_followers 
                    FOREIGN KEY (artist) REFERENCES users(id)
            )`
    );

    db.run(`
        CREATE TABLE IF NOT EXISTS states
            (
                user BIGINT NOT NULL,
                command TEXT NOT NULL,
                CONSTRAINT fk_followers
                    FOREIGN KEY (user) REFERENCES users(id)
            )
    `)

    db.close()
})