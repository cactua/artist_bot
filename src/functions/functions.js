const fs = require('fs')
const fn = {}
fs.readdir('./src/functions/fn', function (err, filenames) {
    filenames.forEach(async function (filename) {
        let filePath = "./fn/" + filename
        let functions = await require(filePath)
        for (let key in functions) {
            fn[key] = functions[key]
        }
    })
    exports.default = fn
})