
global.customMessage = {
    cache: [],
    isRunning: false,
    send: function (user, message) {
        this.cache.push({ user: user, message: message, type: 'text' })
        if (!this.isRunning) {
            this.isRunning = true
            this.run()
        }
    },
    run: function () {
        let self = this
        message = this.cache.shift()
        if (!message) return;
        if (message.type === "text") {
            try {
                setTimeout(function () {
                    bot.sendMessage(message.user, message.message).then(function () {
                        if (self.cache.length === 0) return self.isRunning = false
                        self.run()
                    })
                }, 10)
            } catch (e) {
                //
            }
        }
    }
}