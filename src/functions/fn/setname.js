const setName = async function (user, name) {
    if (await fn.artistExists(name)) {
        try {
            bot.sendMessage(user.id, `Artist name ${name} is already in use :C`)
        } catch (e) { }
        return
    }
    var query = db.prepare("UPDATE users SET artist_name = ? WHERE id = ?")
    query.run([name, user.id], function () {
        try {
            bot.sendMessage(user.id, `Your new artist name is: ${name}\n\nUse this link to allow users to follow you:\n http://t.me/artists_status_bot?start=${name}`)
            fn.removeState(user)
            query.finalize()
            //db.close()
        } catch (e) { }
    })
}

exports.setName = setName