const addUser = async function (user, print_messages = true) {
    if (await fn.userExists(user)) {
        if (print_messages) {
            try {
                bot.sendMessage(user.id, 'Welcome back');
            } catch (e) { }
        }
        return false
    }
    db.serialize(function () {
        db.run("INSERT INTO users (id,telegram_tag) VALUES (?,?)", [user.id, user.username])
        try {
            try {
                bot.sendMessage(user.id, `Thank you for joining.\n\n If you're an artist use the command /setname to generate an invite link`);
            } catch (e) { }
        } catch (e) { }
        console.log(user.username + " Joined")
        //db.close()
    })
}

exports.addUser = addUser