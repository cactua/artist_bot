const followArtist = async function (user, artist_name) {
    await fn.addUser(user, false)
    let artist = await fn.getArtist(artist_name)

    if (!artist) {
        try {
            bot.sendMessage(user.id, 'Artist not found')
        } catch (e) { }
        return
    }

    if (artist.id === user.id) {
        try {
            bot.sendMessage(user.id, 'You can\'t follow yourself')
        } catch (e) { }
        return
    }

    db.serialize(function () {
        var query = db.prepare('SELECT rowid FROM followers WHERE user = ? AND artist = ?')
        query.get([user.id, artist.id], function (err, row) {
            if (row) {
                try {
                    bot.sendMessage(user.id, 'You already follow this artist')
                } catch (e) { }
                query.finalize()
                return
            }
            try {
                db.run(`INSERT INTO followers (user,artist) VALUES (${user.id}, ${artist.id})`, [], function () {
                })
            } catch (e) {
                console.log(e)
            }
            try {
                bot.sendMessage(user.id, `You now follow ${artist.artist_name}`)
            } catch (e) { }
        })
    })
}

exports.followArtist = followArtist