const addState = async function (user, command) {
    const self = this
    return new Promise(async function (resolve) {
        let check = await self.haveState(user)
        if (check) {
            try {
                bot.sendMessage(user.id, 'You have a running command, use /stop to exit')
            } catch (e) { }
            resolve(false)
        }
        let query = db.prepare('INSERT INTO states (user,command) VALUES (?,?)')
        query.run(user.id, command)
        query.finalize()
        //db.close()
        resolve(true)
    })
}

exports.addState = addState