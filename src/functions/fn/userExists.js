const userExists = function (user) {
    return new Promise(function (resolve) {
        let query = "SELECT id FROM users WHERE id = ?"
        db.get(query, [user.id], (err, rows) => {
            //db.close()
            return resolve(rows)
        })
    })
}

exports.userExists = userExists