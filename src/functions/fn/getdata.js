const getData = function (user) {
    const promise = new Promise(function (resolve) {
        let query = db.prepare("SELECT * FROM users WHERE id = ?")
        db.serialize(function () {
            query.all(user.id, function (err, row) {
                resolve(row[0])
                query.finalize()
                //db.close()
            })
        })
    })
    return promise
}

exports.getData = getData