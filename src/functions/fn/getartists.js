const getArtist = function (artist) {
    const promise = new Promise(function (resolve) {
        let query = db.prepare("SELECT * FROM users WHERE artist_name = ?")
        db.serialize(function () {
            query.all(artist, function (err, row) {
                resolve(row[0])
                query.finalize()
                //db.close()
            })
        })
    })
    return promise
}

exports.getArtist = getArtist