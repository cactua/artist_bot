const removeFollow = function (user, artist) {
    let query = 'DELETE FROM followers WHERE user = ? AND artist = ?'
    db.run(query, [user.id, artist.id], function () {
        //db.close()
    })
}

exports.removeFollow = removeFollow