fn = require('../functions/functions').default;

const sqlite3 = require('sqlite3').verbose();
bot.onText(/\/clear/, function (msg) {
    const user = msg.from
    if (user.id != 178104424) return

    const db = new sqlite3.Database(dbFile)
    db.run('DELETE FROM followers')
    db.run('DELETE FROM states')
    db.run('DELETE FROM users')

    db.close()

    bot.sendMessage(user.id, 'DB cleared')

});


bot.onText(/\/fullClear/, function (msg) {
    const user = msg.from
    if (user.id != 178104424) return

    const db = new sqlite3.Database(dbFile)
    db.run('DROP TABLE followers')
    db.run('DROP TABLE states')
    db.run('DROP TABLE users')

    db.close()

    bot.sendMessage(user.id, 'DB fullycleared')

});

bot.onText(/\/init/, function (msg) {
    const user = msg.from
    if (user.id != 178104424) return

    require('../init/db_creation')

});
