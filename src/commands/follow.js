fn = require('../functions/functions').default;

bot.onText(/\/start (.+)/, (msg, match) => {
    const user = msg.chat;
    const artist = match[1];
    fn.followArtist(user, artist)
});
